import Home from './Home.js'
import OrderDelivery from './OrderDelivery/index.js'
import Restaurant from './Restaurant.js'
import Search from './Search'

export {
  Home,
  OrderDelivery,
  Restaurant,
};