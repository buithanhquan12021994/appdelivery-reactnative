import React, {useEffect, useState} from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native' 

// component
import Map from './Map'

const index = ({navigation, route}) => {

  const [restaurant, setRestaurant] = useState(null)
  const [streetName, setStreetName] = useState("")
  const [fromLocation, setFromLocation] = useState(null);
  const [toLocation, setToLocation] = useState(null)
  const [region, setRegion] = useState(null)

  useEffect(() => {
    const {restaurant, currentLocation } = route.params;

    let fromLoc = currentLocation.gps;
    let toLoc = restaurant.location;
    let street = currentLocation.streetName;  

    let mapRegion = {
      latitude: (fromLoc.latitude + toLoc.latitude) / 2,
      longitude: (fromLoc.longitude + toLoc.longitude) / 2,
      latitudeDelta: Math.abs(fromLoc.latitude - toLoc.latitude) * 2,
      longitudeDelta: Math.abs(fromLoc.longitude - toLoc.longitude) * 2
    }

    // update state
    setRestaurant(restaurant)
    setStreetName(street)
    setFromLocation(fromLoc)
    setToLocation(toLoc)
    setRegion(mapRegion)
  }, [])


  return (
    <View style={styles.container}>
      <Map 
        region={region} 
        toLocation={toLocation} 
        fromLocation={fromLocation} 
        streetName={streetName} 
        restaurant={restaurant} 
        navigation={navigation}
        updateRegion={(data) => setRegion(data) }
        />
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default index;