import React, {useEffect, useState, useRef } from 'react'
import { 
  StyleSheet,
  View, 
  Text,
  Image, 
  TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import { icons, images, COLORS, SIZES, FONTS, GOOGLE_API_KEY } from '../../constants'
import MapViewDirections  from 'react-native-maps-directions'


const Map = ({region, toLocation, fromLocation, streetName, restaurant, navigation, updateRegion}) => {
  const mapView = useRef()

  const[duration, setDuration] = useState(0)
  const[isReady, setIsReady] = useState(false)
  const[angle, setAngle] = useState(0)


  // component
  const destinationMarker = () => {
    return (
      <>
        { 
          toLocation ? 
          <Marker coordinate={toLocation}>
            <View style={styles.markerContainer}>
              <View style={styles.markerDot}>
                <Image
                  source={icons.pin}
                  style={styles.markerImg}
                />
              </View>
            </View>
          </Marker>
          : <></>}
      </>
    )
  }

  // component
  const carIcon = () => {
    return (
      <>
        { fromLocation ? 
          <Marker
            coordinate={fromLocation}
            anchor={{ x: 0.5, y: 0.5}}
            flat={true}
            rotation={angle}
            >
            <Image 
              source={icons.car}
              style={styles.markerCarImg}
            /> 
          </Marker>
          : <></>
        }
      </>
    ) 
  }

  // component
  const renderDestinationHeader = () => (
    <View
      style={{
        position: 'absolute',
        top: 50,
        left: 0, 
        right: 0,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: SIZES.width * 0.9,
          paddingVertical: SIZES.padding,
          paddingHorizontal: SIZES.padding * 2,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.white
        }}
      >
        <Image 
          source={icons.red_pin}
          style={{
            width: 30,
            height: 30,
            marginRight: SIZES.padding
          }}
        />
        <View style={{ flex: 1}}>
          <Text style={{ ...FONTS.body3 }}>{streetName}</Text>
        </View>

        <Text style={{ ...FONTS.body3}}>{Math.ceil(duration)} mins</Text>
      </View>
    </View>
  )

  // component
  const renderDelivery = () => (
    <View style={{
      position: 'absolute',
      bottom: 50,
      left: 0,
      right: 0,
      alignItems: 'center',
      justifyContent: 'center'
    }}>
      <View
        style={{ 
          width: SIZES.width * 0.9,
          paddingVertical: SIZES.padding * 3,
          paddingHorizontal: SIZES.padding * 2,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.white
        }}
      > 
        {/* Avatar - Name - Restaurant Name - Rating */}
        <View style={{ flexDirection: 'row', alignItems: 'center'}}>
          {/* AVATAR */}
          <Image 
            source={restaurant?.courier.avatar}
            style={{
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          />
          
          <View style={{ flex: 1, marginLeft: SIZES.padding }}>
            {/* NAME & RATING */}
            <View style={{ flexDirection: 'row' , justifyContent:'space-between'}}>
              <Text style={{ ...FONTS.body3 }}>{restaurant?.courier.name}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Image 
                  source={icons.star}
                  style={{ width: 18, height: 18, tintColor: COLORS.primary, marginRight: SIZES.padding }}
                />
                <Text style={{ ...FONTS.body3}}>{restaurant?.rating}</Text>
              </View>
            </View>

            {/* Restaurant */}
            <Text style={{ color: COLORS.darkgray, ...FONTS.body4}}>{restaurant?.name}</Text>
          </View>
        </View>

        <View style={{ flexDirection: 'row', justifyContent:'space-between', marginTop: SIZES.padding }}>
            <TouchableOpacity 
              style={{
                backgroundColor: COLORS.primary, 
                height: 50,
                flex: 1,
                marginRight: 10,
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center'   
              }}
              onPress={() => navigation.navigate('Home')}
              >      
              <Text style={{ ...FONTS.body2, color: COLORS.white}}>Call</Text> 
            </TouchableOpacity>

            <TouchableOpacity 
              style={{ 
                height: 50,
                flex: 1,
                marginLeft: 10,
                backgroundColor: COLORS.darkgray, 
                paddingHorizontal: SIZES.padding, 
                paddingVertical: SIZES.padding,
                borderRadius: 10,
                border: '1px solid rgba(0, 0, 0, 0.05)', 
                alignItems: 'center',
                justifyContent: 'center'
              }}
              onPress={() => navigation.goBack()}
              >
             
              <Text style={{ ...FONTS.body2, color: COLORS.white }}>Cancel</Text>
              
            </TouchableOpacity>
        </View>
      </View>
    </View>
  )

  // component
  const renderButtonZoom = () => (
    <View style={{position: 'absolute', bottom: SIZES.height * 0.35, right: SIZES.padding * 2, width: 60, height: 130, justifyContent: 'space-between'}}>
      {/* Zoom in */}
      <TouchableOpacity
        style={{ 
          width: 60, height: 60, borderRadius: 30, backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center'
        }}
        onPress={() => zoomIn()}
        >
        <Text style={{ ...FONTS.body1}}>+</Text>
      </TouchableOpacity>

      {/* Zoom out */}
      <TouchableOpacity
        style={{ 
          width: 60, height: 60, borderRadius: 30, backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center'
        }}
        onPress={() => zoomOut()}
        >
        <Text style={{ ...FONTS.body1}}>-</Text>
      </TouchableOpacity>
    </View>
  )

  // function
  function calculateAngle(coordinates) {
    let startLat = coordinates[0]["latitude"]
    let startLoc = coordinates[0]["longitude"]
    let endLat = coordinates[1]["latitued"]
    let endLoc = coordinates[1]["longitude"]
    let dx = endLat - startLat;
    let dy = endLoc - startLoc;

    return Math.atan2(dy, dx) * 100 / Math.PI
  }

  // function
  function zoomIn() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta / 2,
      longitudeDelta: region.longitudeDelta / 2,
    }

    updateRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }

  // function
  function zoomOut() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta * 2,
      longitudeDelta: region.longitudeDelta * 2,
    }

    updateRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }

  return (
    <View style={styles.container}>
      <MapView 
        style={styles.map}
        ref={mapView}
        provider={PROVIDER_GOOGLE}
        region={region}
      >
        <MapViewDirections
          origin={fromLocation}
          destination={toLocation}
          apikey={GOOGLE_API_KEY}
          strokeWidth={5}
          strokeColor={COLORS.primary}
          optimizeWaypoints={true}
          onReady={result => {
            setDuration(result.duration)

            if (!isReady) {
              // fit route to maps
              mapView.current.fitToCoordinates(result.coordinates, {
                edgePadding: { 
                  right: (SIZES.width / 20),
                  left: (SIZES.width / 20),
                  bottom: (SIZES.height / 4),
                  top: (SIZES.height / 8)
                }
              })

              //reposition the car
              let nextLoc = {
                latitude: result.coordinates[0]["latitude"],
                longitude: result.coordinates[0]["longitude"]
              }

              if (result.coordinates.length >= 2) {
                let angle = calculateAngle(result.coordinates)
                setAngle(angle)
              }

              setFromLocation(nextLoc)
              setIsReady(true)
            }
          }}
        ></MapViewDirections>
        {destinationMarker()}
        {carIcon()}
      </MapView>
      {renderDestinationHeader()}
      {renderDelivery()}
      {renderButtonZoom()}
    </View>
  )
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  map: { 
    height: SIZES.height,
    width: SIZES.width,
  },
  markerContainer: {
    paddingVertical: SIZES.padding / 2,
    paddingHorizontal: SIZES.padding / 2,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white,
  },
  markerDot: {
    height: 20, 
    width: 20,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary
  },
  markerImg: {
    height: 10,
    width: 10,
    tintColor: COLORS.white
  },
  markerCarImg: {
    height: 30,
    width: 30,
  }
})


export default Map;