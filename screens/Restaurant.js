import React, {useState, useEffect} from 'react';
import { 
  StyleSheet,
  View, 
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image, 
  Animated,
 } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper';
import { icons, COLORS, SIZES, FONTS } from '../constants'

const Restaurant = ({route, navigation}) => {

  const scrollX = new Animated.Value(0);
  const[restaurant, setRestaurant] = useState(null);
  const[currentLocation, setCurrentLocation] = useState();
  const[orderItems, setOrderItems] = useState([]);

  useEffect(() => {
    let {item, currentLocation} = route.params;
  
    // set restaurant 
    setRestaurant(item);
    // setC current location
    setCurrentLocation(currentLocation)
    
  }, [route.params])


  // Edit order [ PLUS | SUBTRACT ]
  function editOrder(action, menuId, price) {
    if (action == "+") {
      let orderList = orderItems.slice();
      let item = orderList.filter(a => a.menuId == menuId);

      if (item.length) {
        let newQty = item[0].qty + 1
        item[0].qty = newQty;
        item[0].total = item[0].qty * price; 
      } else {
        const newItem = { 
          menuId: menuId,
          qty: 1, 
          price: price,
          total: price
        }
        orderList.push(newItem)
      }

      setOrderItems(orderList);
    } else if (action == "-") {
      let orderList = orderItems.slice();
      let findOrderItem = orderList.filter(i => i.menuId == menuId);
      if (findOrderItem.length) { 
        if (findOrderItem[0].qty <= 1) { // because it's 1, after remove => filter menu is different menuID
          orderList = orderList.filter(i => i.menuId != menuId);
          setOrderItems(orderList);
          return;
        } else {
          let newQty = findOrderItem[0].qty - 1;
          findOrderItem[0].qty = newQty;
          findOrderItem[0].total = findOrderItem[0].total - findOrderItem[0].price;
        }
      }
      setOrderItems(orderList);
    }
    console.log(orderItems);
  }

  // get total qty base on MenuID
  function getOrderQty(menuId) {
    let filterMenu = orderItems.find(i => i.menuId == menuId);
    return typeof filterMenu != 'undefined' ? filterMenu.qty : 0; 
  }

  //
  function getBasketItemCount() {
    return orderItems.reduce((initVal, item) => initVal + (item.qty || 0), 0);
  }
  // 
  function totalPrice() {
    return orderItems.reduce((initVal, item) => initVal + (item.total || 0), 0);
  }

  return (
    <SafeAreaView style={{ height: SIZES.height }}>
      <RenderHeader navigation={navigation} restaurant={restaurant} />
      <RenderFood 
        restaurant={restaurant} 
        scrollX={scrollX} 
        editOrder={editOrder} 
        getOrderQty={getOrderQty}/>
      <RenderOrder
        getBasketItemCount={getBasketItemCount}
        totalPrice={totalPrice}
        navigation={navigation}
        restaurant={restaurant}
        currentLocation={currentLocation}
      />
    </SafeAreaView>
  )
}

const RenderHeader = ({navigation, restaurant}) => {
 
  return (
    <View style={styles.headerContainer}>
      <TouchableOpacity
        style={{ ...styles.touchImgHeader, paddingLeft: SIZES.padding}}
        onPress={() => navigation.goBack()}      
        >
        <Image 
          source={icons.back}
          resizeMode="contain"
          style={styles.imageIconHeader}
        />
      </TouchableOpacity>

      {/* Name Restaurant */}
      <View style={styles.restaurantNameBox} >
        <Text style={{...FONTS.h4, ...FONTS.medium}}>{restaurant?.name}</Text>
      </View>
      
      <TouchableOpacity
        style={{ ...styles.touchImgHeader, paddingRight: SIZES.padding}}
        >
        <Image 
          source={icons.list}
          resizeMode="contain"
          style={styles.imageIconHeader}
        />
      </TouchableOpacity>
    </View>
  )
}

// Render food component & dot component via Animated
const RenderFood = ({restaurant, scrollX, getOrderQty, editOrder}) => {
  const dotPosition = new Animated.divide(scrollX, SIZES.width);
  return (
    <>
      <Animated.ScrollView
        horizontal
        pagingEnabled
        scrollEventThrottle={16}
        snapToAlignment="center"
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: {x: scrollX} } }
        ], { useNativeDriver: false})}
      >
      {   
        restaurant?.menu?.map((itemMenu, index) => (
            <View key={index}
              style={{alignItems: 'center'}}>
              <View style={{ height: SIZES.height * 0.35}} key={itemMenu.id}>
                <Image
                  key={`image-${itemMenu.id}`} 
                  source={itemMenu.photo}
                  resizeMode="cover"
                  style={{
                    width: SIZES.width,
                    height: '100%'
                  }}/>
                {/* Quantity */}
                <View
                  style={{
                    position: 'absolute',
                    bottom: -20,
                    width: SIZES.width,
                    height: 50,
                    justifyContent: 'center',
                    flexDirection: 'row', 
                  }}>
                  {/* Minus */}
                    <TouchableOpacity
                      style={{
                        width: 50,
                        backgroundColor: COLORS.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderTopLeftRadius: 25,
                        borderBottomLeftRadius: 25,
                      }}
                      onPress={() => editOrder("-", itemMenu.menuId, itemMenu.price)}
                      >
                      <Text style={{...FONTS.semibold, ...FONTS.body3}}>-</Text>
                    </TouchableOpacity>

                    {/* Number quantity */}
                    <View 
                      style={{
                        width: 50, 
                        backgroundColor: COLORS.white,
                        alignItems: 'center',
                        justifyContent: 'center'
                      }}
                      >
                      <Text style={{...FONTS.h2}}>
                        {getOrderQty(itemMenu.menuId)}
                      </Text>
                    </View>

                    {/* PLUS */}
                    <TouchableOpacity
                      style={{
                        width: 50, 
                        backgroundColor: COLORS.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderBottomEndRadius: 25,
                        borderTopRightRadius: 25
                      }}
                      onPress={() => editOrder("+", itemMenu.menuId, itemMenu.price)}
                      >
                      <Text style={{...FONTS.semibold, ...FONTS.body3}}>+</Text>
                    </TouchableOpacity>
                  </View>
              </View>
              {/* Name and description */}
              <View
                style={{
                  width: SIZES.width,
                  alignItems: 'center',
                  marginTop: 15,
                  paddingHorizontal: SIZES.padding * 2
                }}
              >
                <Text style={{ marginVertical: 10, textAlign: 'center', ...FONTS.h3}}>{itemMenu.name}</Text>
                <Text style={{ ...FONTS.body3}}>{itemMenu.description}</Text>
              </View>
              {/* Calories */}
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10
                }}
              >
                <Image
                  source={icons.fire}
                  style={{
                    height: 20,
                    width: 20, 
                    marginRight: 10
                  }}
                />

                <Text style={{ ...FONTS.body3, color: COLORS.darkgray, ...FONTS.medium }}>{ itemMenu.calories.toFixed(2) } cal</Text>
              </View>
            </View>
        )) 
      }
      </Animated.ScrollView>

      {/* Dot View */}
      <View style={{ height: 30, marginVertical: 10 }}>
          <View
              style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: SIZES.padding
              }}
          >
              {restaurant?.menu.map((item, index) => {

                  const opacity = dotPosition.interpolate({
                      inputRange: [index - 1, index, index + 1],
                      outputRange: [0.3, 1, 0.3],
                      extrapolate: "clamp"
                  })

                  const dotSize = dotPosition.interpolate({
                      inputRange: [index - 1, index, index + 1],
                      outputRange: [SIZES.base * 0.8, 10, SIZES.base * 0.8],
                      extrapolate: "clamp"
                  })

                  const dotColor = dotPosition.interpolate({
                      inputRange: [index - 1, index, index + 1],
                      outputRange: [COLORS.darkgray, COLORS.primary, COLORS.darkgray],
                      extrapolate: "clamp"
                  })

                  return (
                      <Animated.View
                          key={`dot-${index}`}
                          opacity={opacity}
                          style={{
                              borderRadius: SIZES.radius,
                              marginHorizontal: 6,
                              width: dotSize,
                              height: dotSize,
                              backgroundColor: dotColor
                          }}
                      />
                  )
              })}
          </View>
      </View>
    </>
  );
}

// render Order component
const RenderOrder = ({getBasketItemCount, totalPrice, navigation, restaurant, currentLocation}) => {
  return (
    <View
      style={{
        backgroundColor: COLORS.white,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: SIZES.padding * 2,
          paddingHorizontal: SIZES.padding * 2,
          borderBottomColor: COLORS.lightGray2,
          borderBottomWidth: 1
        }}
        >
          <Text style={{ ...FONTS.h3 }}>{getBasketItemCount()} items in Cart</Text>
          <Text style={{ ...FONTS.h3 }}>${totalPrice()}</Text>
      </View> 

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: SIZES.padding * 2,
          paddingHorizontal: SIZES.padding * 2,
        }}
        >
        <View style={{ flexDirection: 'row' }}>
          <Image 
            source={icons.pin}
            resizeMode="contain"
            style={{
              width: 28, 
              height: 28,
              tintColor: COLORS.darkgray 
            }}
          />
          <Text style={{ marginLeft: SIZES.padding, ...FONTS.h4 }}>Location</Text>
        </View>

        <View style={{ flexDirection: 'row' }}> 
          <Image 
            source={icons.master_card}
            resizeMode="contain"
            style={{
              width: 28, 
              height: 28,
              tintColor: COLORS.darkgray 
            }}
          />

          <Text style={{ marginLeft: SIZES.padding, ...FONTS.h4}}>8888</Text>
        </View>   
     </View>

     {/* Order button */}
      <View
        style={{
          padding: SIZES.padding * 2,
          alignItems: 'center',
          justifyContent: 'center'
        }}
        >
        <TouchableOpacity
          style={{
            width: SIZES.width * 0.8,
            padding: SIZES.padding / 2,
            backgroundColor: COLORS.primary, 
            alignItems: 'center',
            borderRadius: SIZES.radius
          }}
          onPress={() => navigation.navigate("OrderDelivery", {
            restaurant: restaurant,
            currentLocation: currentLocation
          })}
          >
          <Text style={{ color: COLORS.white, ... FONTS.h2}}>Order</Text> 
        </TouchableOpacity>
      </View>
      {
        isIphoneX() && 
          <View
            style={{
              position: 'absolute',
              bottom: -34,
              left: 0,
              right: 0,
              height: 34,
              backgroundColor: COLORS.white
            }}
          ></View>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row', 
    justifyContent:'space-between', 
    height: 50, 
    alignItems: 'center', 
    marginTop: SIZES.padding * 2,
    marginVertical: 10
  },
  touchImgHeader: {
    width: 50, 
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageIconHeader: {
    width: 30, 
    height: 30
  },

  restaurantNameBox: {
    width: '60%',
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray5,
    borderRadius: SIZES.radius
  }
})

export default Restaurant;