import React, {useState, useEffect} from 'react';

import { COLORS, icons, FONTS, SIZES } from '../constants'

import { 
  Platform,
  View, 
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  FlatList
 } from 'react-native'

 import {
  initialCurrentLocation,
  restaurantData,
  affordable,
  fairPrice,
  expensive,
  categoryData} from  '../dump/data'

// Header
const RenderHeader = (props) => {
  return (
    <View style={{ flexDirection: 'row', justifyContent:'space-between', height: 50, alignItems: 'center', marginTop: SIZES.padding}}>
      <TouchableOpacity
        style={{
          width: 50,
          paddingLeft: SIZES.padding * 2,
          justifyContent: 'center'
        }}        
      >
        <Image 
          source={icons.nearby}
          resizeMode="contain"
          style={{
            width: 30, 
            height: 30
          }}
        />
      </TouchableOpacity>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={{
          width: '70%',
          height: '100%',
          backgroundColor: COLORS.lightGray3,
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: SIZES.radius
        }}>
          <Text style={{...FONTS.h3}}>{props.location.streetName}</Text>
        </View>
      </View>
        
      <TouchableOpacity
        style={{
          width: 50,
          paddingRight: SIZES.padding * 2,
          justifyContent: 'center'
        }}
      >
        <Image 
          source={icons.basket}
          resizeMode="contain"
          style={{
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity>
    
    </View>
  )
}

// Main Categories 
const RenderMainCategories = (props) => {
 
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        padding: SIZES.padding,
        paddingBottom: SIZES.padding * 2,
        backgroundColor: (props.category?.id == item.id) ? COLORS.primary : COLORS.white,
        borderRadius: SIZES.radius,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: SIZES.padding,
        ...styles.shadow
      }}
      onPress={() => props.selectItemCategory(item)}
    >
      <View
        style={{
          height: 40,  
          width: 40,
          borderRadius: 20,
          alignItems: 'center', 
          justifyContent: 'center',
          backgroundColor: (props.category?.id == item.id) ? COLORS.white : COLORS.lightGray4
        }}
      >
        <Image 
          source={item.icon}
          resizeMode="contain"
          style={{
            height: 30,
            width: 30
          }}
        />
        
      </View>
      <Text 
        style={{
          marginTop: SIZES.padding,
          color: (props.category?.id == item.id) ? COLORS.white : COLORS.black,
          ...FONTS.body5
        }}
      >
        {item.name}
      </Text>
    </TouchableOpacity>
  );


  return (
    <View style={{ padding: SIZES.padding * 2}}>
      <Text style={{...FONTS.h1 }}>Main</Text>
      <Text style={{...FONTS.h1 }}>Categories</Text>

      <FlatList
        data={props.categories}
        horizontal
        keyExtractor={(item) =>  item.id.toString()}
        renderItem={renderItem}
        contentContainerStyle={{ paddingVertical: SIZES.padding * 2}}
      ></FlatList>
    </View>
  )
}

// Render Restaurant 
const RenderRestaurants = ({navigation, restaurants, currentLocation}) => {

  const getNameCategory = (categoryID) => {
    let find = categoryData.find(itemCategory => itemCategory.id == categoryID)
    return find ? find.name : ''
  }

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        marginBottom: SIZES.padding * 2, 
      }}
      onPress={() => navigation.navigate("Restaurant", {
        item, 
        currentLocation
      })}
    >
      <View>
        <Image 
          source={item.photo}
          resizeMode="cover"
          style={{
            width: '100%',
            height: 200,
            borderRadius: SIZES.radius 
          }}
        />
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            height: 50,
            width: SIZES.width * 0.3,
            backgroundColor: COLORS.white,
            borderTopRightRadius: SIZES.radius,
            borderBottomLeftRadius: SIZES.radius,
            alignItems: 'center',
            justifyContent: 'center',
            ...styles.shadow
          }}
        >
          <Text style={{...styles.h4, ...FONTS.bold}}>{item.duration}</Text>
        </View>
      </View>
      {/* Restaurant Information */}
      <Text style={{...FONTS.body2, ...FONTS.medium, marginTop: SIZES.padding / 2 }}>{item.name}</Text>  
      <View
        style={{
          marginTop: SIZES.padding,
          flexDirection: 'row'
        }}
      >
        {/* Rating */}
        <Image 
          source={icons.star}
          style={{
            height: 20,
            width: 20,
            tintColor: COLORS.primary,
            marginRight: 10
          }}
        />
        <Text style={{...FONTS.body3 }}>{item.rating}</Text>

        {/* Categories */}
        <View
          style={{
            flexDirection: 'row',
            marginLeft: 10,
          }}
        >
          {
            item.categories.map(itemCategory => (
              <View
                style={{ flexDirection: 'row' }}
                key={itemCategory.toString()}
              >
                <Text style={{ ...FONTS.body3 }}>{getNameCategory(itemCategory)}</Text>
                <Text style={{ ...FONTS.h3, color: COLORS.darkgray }}> . </Text>
              </View>
            ))
          }
        </View>

        {/* Price */}
        {
          [affordable,fairPrice,expensive].map(priceRating => (
            <Text
              key={priceRating.toString()}
              style={{
                ...FONTS.body3,
                color: (priceRating <= item.priceRating) ? COLORS.black :  COLORS.darkgray
              }}
              >
                $
              </Text>
          ))
        }
      </View>
    </TouchableOpacity>
  )
  return (
    <FlatList
      data={restaurants}
      keyExtractor={(item) => item.id.toString()}
      renderItem={renderItem}
      contentContainerStyle={{
        paddingHorizontal: SIZES.padding * 2,
        paddingBottom: 30
      }}
    ></FlatList>
  )
}

// SCREEN HOME PAGE
const Home = ({navigation}) => {
  const[categories, setCategories] = useState(categoryData);
  const[selectedCategory, setSelectedCategory] = useState(null);
  const[restaurants, setRestaurants] = useState(restaurantData)
  const[currentLocation, setCurrentLocation] = useState(initialCurrentLocation)

  const chooseCategory = (category) => {
    // filter restaurant
    let restaurantFilterByCategoryID = restaurantData.filter(itemRestaurant => itemRestaurant.categories.includes(category.id))

    // update state Category 
    setSelectedCategory(category)
    
    // update state restaurant
    setRestaurants(restaurantFilterByCategoryID)
  }
 

  return (
    <SafeAreaView style={styles.container}>
      <RenderHeader location={currentLocation}/>
      <RenderMainCategories 
        categories={categories} 
        category={selectedCategory}
        selectItemCategory={(itemCategory) => chooseCategory(itemCategory)} 
      />
      <RenderRestaurants 
        restaurants={restaurants}
        navigation={navigation}
        currentLocation={currentLocation}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    backgroundColor: COLORS.lightGray4
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0, 
      height: 3
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1
  }
})

export default Home;