**Basic learning**


# Project
App delivery by react-
# Requirement GLOBAL ENV
1. Expo cli (install [here](https://docs.expo.io/get-started/installation/))
2. NodeJS and NPM (install [here]([https://link](https://nodejs.org/en/download/)))
1. @react-native-community/masked-view
2. @react-navigation/bottom-tabs
3. @react-navigation/native
4. @react-navigation/stack
5. react
6. react-dom
7. react-native
8. react-native-easy-grid
9. react-native-gesture-handler
10. react-native-iphone-x-helper
11. react-native-reanimated
12. react-native-safe-area-context
13. react-native-screens
14. react-native-svg
15. react-native-web
  
# How to run
**1. Pull code from git**

**2. Run installation**
```
npm install
```
**2.1 Set up React-native-maps**
```
expo install react-native-maps
```
Go to **app.json** file

Add config like that: **"android.config.googleMaps.apiKey"** = GoogleApiKey


**2.2 Set up react-native-maps-directions**
```
expo install react-native-maps-directions

```

Get API KEY from google cloud 

Put API KEY to **./constants/map.js**


**3. Start EXPO**

1. Start server: `npm start`
2. Start Web: `npm run web`
3. Start Android: `npm run android`
4. Start IOS: `npm run ios`
   
**4. Run on real device**

*Using Expo client on **CH-PLAY** for Android and **APP STORE** for IPhone*

Open app and **Scan QR CODE** to run development













