import React, {useEffect} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import {Home, OrderDelivery, Restaurant} from './screens'
import Tabs from './navigation/tab'

// Adding font
import * as Font from 'expo-font';
import { AppLoading } from 'expo'


const Routes = [
  {
    name: 'Home',
    component: Tabs
  },
  {
    name: 'Restaurant',
    component: Restaurant
  },
  {
    name: 'OrderDelivery',
    component: OrderDelivery
  }, 
];

const Stack = createStackNavigator();

const App = () => {
  useEffect(() => {
    Font.loadAsync({
      'Roboto-Bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'Roboto-Black': require('./assets/fonts/Roboto-Black.ttf'),
      'Roboto-Italic': require('./assets/fonts/Roboto-Italic.ttf'),
      'Roboto-Regular': require('./assets/fonts/Roboto-Regular.ttf')
    })
  }, [])

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
        initialRouteName={"Home"}
      >
        {Routes.map(i => (
          <Stack.Screen 
            name={i.name} 
            component={i.component}
            key={i.name}></Stack.Screen>
        ))}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App